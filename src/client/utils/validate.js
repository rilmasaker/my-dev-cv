
const validate = (values) => {
  const errors = {};
  if (!values.Name) {
    errors.Name = 'Required';
  }
  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }
  if (!values.Surname) {
    errors.Surname = 'Required';
  }
  if (!values.Terms) {
    errors.Terms = 'Required';
  }

  return errors;
};

export default validate;
