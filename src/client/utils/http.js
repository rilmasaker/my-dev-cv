import { useoPath } from './configuration';

export function get(url) {
  return fetch(url).then(response => response.json());
}

export function post(url, todo, date) {
  return fetch(url, {
    method: 'POST',
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ note: { content: todo, deadline: date } })
  }).then(res => res.json());
}

export function put(id, done) {
  return fetch(`${useoPath}/${id}/${done ? 'uncompleted' : 'completed'}`, {
    method: 'PUT',
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
  }).then(res => res.json())
    .then(res => console.log(res));
}

export function destroy(id) {
  return fetch(`${useoPath}/${id}`, {
    method: 'DELETE',
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
  }).then(res => res.json())
    .then(res => console.log(res));
}

export function loadMore(page) {
  return fetch(`https://useo-notes.herokuapp.com/notes?page=${page}`).then(response => response.json());
}
