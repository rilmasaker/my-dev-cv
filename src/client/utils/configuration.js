export const useoPath = 'https://useo-notes.herokuapp.com/notes';

export default {
  GET_TASK: useoPath,
  ADD_TASK: useoPath,
  DELETE_TASK: useoPath,
};
