export function isDefined(variable) {
  return typeof variable !== 'undefined' && variable !== null;
}

export function Optional(value) {
  return {
    or: orValue => (isDefined(value) ? value : orValue)
  };
}
