/* eslint-disable no-shadow */
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import Typed from 'react-typed';

import Table from '../common/Table';
import TableHeader from './components/TableHeader';
import TableRow from './components/TableRow';
import TableSubHeader from './components/SubHeader';
import SnackbarInfo from './components/SnackBarInfo';
import { getTask, loadTask } from './actions/todo-action';

class ToDo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 2
    };
  }

  componentWillMount() {
    const { getTask } = this.props;
    getTask();
  }

  render() {
    const { loadTask, list } = this.props;
    const { page } = this.state;
    return (
      <Fragment>
        <div className="main-containter">
          <span
            className="title typewriter"
          >
            <Typed
              strings={['Please wait, slow server.']}
              typeSpeed={200}
            />
          </span>
          <Table
            className="rest-table"
            row={TableRow}
            header={TableHeader}
            subHeader={TableSubHeader}
            list={list}
            style={{
              borderLeft: '1px solid rgba(224, 224, 224, 1)',
              borderRight: '1px solid rgba(224, 224, 224, 1)',
            }}
          />
          <Button
            className="button rest-button"
            onClick={() => {
              loadTask(page);
              this.setState({ page: page + 1 });
            }}
            variant="contained"
            color="primary"
            style={{
              backgroundColor: 'rgb(253,106,2)'
            }}
          >
            <i className="fas fa-hand-point-right" />
                        Load More
          </Button>
        </div>
        <SnackbarInfo />
      </Fragment>
    );
  }
}

ToDo.propTypes = {
  list: PropTypes.array.isRequired,
  getTask: PropTypes.func.isRequired,
  loadTask: PropTypes.func.isRequired
};

export default connect(state => ({ list: state.toDo }), { getTask, loadTask })(ToDo);
