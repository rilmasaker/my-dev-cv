import { ACTIONS } from '../reducers/snackBar-reducer';

export function openSnackBar(message) {
  return {
    type: ACTIONS.OPEN,
    message
  };
}

export function closeSnackBar() {
  return { type: ACTIONS.CLOSE };
}
