import { ACTIONS } from '../reducers/toDo-reducer';

export function setTodos(todos) {
  return { type: ACTIONS.SET, todos };
}

export function toggleDone(id) {
  return { type: ACTIONS.TOGGLE, id };
}

export function addTodos(newTodos) {
  return { type: ACTIONS.ADD, newTodos };
}

export function deleteTask(deleteId) {
  return { type: ACTIONS.DELETE, deleteId };
}

export function addTodosArray(newTodos) {
  return { type: ACTIONS.ADD_ARRAY, newTodos };
}
