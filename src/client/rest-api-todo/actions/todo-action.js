import {
  get, post, put, destroy, loadMore
} from '../../utils/http';
import CONFIG from '../../utils/configuration';
import {
  setTodos, addTodos, toggleDone, deleteTask, addTodosArray
} from './todo-action-creators';
import { openSnackBar } from './snackBar-action-creator';
import { clearPhrase } from './input-action-creators';

export function getTask() {
  return dispatch => get(CONFIG.GET_TASK).then((todos) => {
    dispatch(setTodos(todos.notes));
  });
}

export function addTask(todo, date) {
  return dispatch => post(CONFIG.ADD_TASK, todo, date).then((res) => {
    if (!res.success) {
      dispatch(openSnackBar(res.errors[0]));
    } else {
      dispatch(addTodos(res.note));
    }
    dispatch(clearPhrase());
  });
}

export function destroyTask(id) {
  return dispatch => destroy(id).then(() => {
    dispatch(deleteTask(id));
  });
}

export function toggleTask(done, id) {
  return dispatch => put(id, done).then(() => {
    dispatch(toggleDone(id));
  });
}

export function loadTask(page) {
  return dispatch => loadMore(page).then((todos) => {
    if (todos.notes.length === 0) {
      dispatch(openSnackBar());
    }
    dispatch(addTodosArray(todos.notes));
  });
}
