import { ACTIONS } from '../reducers/input-reducer';

export function setPhrase(phrase) {
  return { type: ACTIONS.SET_VALUE, phrase };
}

export function clearPhrase() {
  return { type: ACTIONS.CLEAR };
}
