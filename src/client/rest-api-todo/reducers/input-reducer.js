export const ACTIONS = {
  SET_VALUE: Symbol('SET_VALUE'),
  CLEAR: Symbol('CLEAR')
};

export default function inputReducer(state = { phrase: '' }, { type, phrase }) {
  switch (type) {
    case ACTIONS.SET_VALUE:
      return { phrase };

    case ACTIONS.CLEAR:
      return {
        ...state,
        phrase: ''
      };

    default:
      return state;
  }
}
