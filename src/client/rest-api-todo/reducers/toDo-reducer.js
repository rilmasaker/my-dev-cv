export const ACTIONS = {
  SET: Symbol('SET'),
  ADD: Symbol('ADD'),
  ADD_ARRAY: Symbol('ADD_ARRAY'),
  DELETE: Symbol('DELETE'),
  TOGGLE: Symbol('TOGGLE')
};

export default function toDoReducer(state = [], {
  type, todos, id, deleteId, newTodos
}) {
  switch (type) {
    case ACTIONS.SET:
      return [...todos];

    case ACTIONS.ADD:
      return [
        ...state,
        newTodos,
      ];

    case ACTIONS.ADD_ARRAY:
      return [
        ...state,
        ...newTodos,
      ];

    case ACTIONS.TOGGLE:
      return state.map((todo) => {
        if (todo.id === id) {
          return {
            ...todo,
            completed: !todo.completed
          };
        }
        return todo;
      });

    case ACTIONS.DELETE:
      return state.filter(todo => todo.id !== deleteId);

    default:
      return state;
  }
}
