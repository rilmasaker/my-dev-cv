export const ACTIONS = {
  OPEN: Symbol('OPEN'),
  CLOSE: Symbol('CLOSE')
};

export default function snackBarReducer(state = { open: false, message: '' }, { type, message }) {
  switch (type) {
    case ACTIONS.OPEN:
      return {
        ...state,
        open: true,
        message
      };

    case ACTIONS.CLOSE:
      return {
        ...state,
        open: false,
        message: ''
      };

    default:
      return state;
  }
}
