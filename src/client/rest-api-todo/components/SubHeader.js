/* eslint-disable no-shadow */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import TableCell from '@material-ui/core/TableCell';
import MaterialTableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';

import { addTask } from '../actions/todo-action';
import Input from './Input';

class TableSubHeader extends Component {
    state = {
      selectedDate: new Date()
    };

    handleDateChange = (e) => {
      e.persist();
      this.setState({ selectedDate: e.target.value });
    };

    render() {
      const { addTask, phrase } = this.props;
      const { selectedDate } = this.state;
      return (
        <MaterialTableRow>
          <TableCell>
            <Input />
          </TableCell>
          <TableCell>
            <TextField
              id="deadline"
              label="Set deadline"
              type="date"
              onChange={this.handleDateChange}
              InputLabelProps={{
                shrink: true
              }}
            />
          </TableCell>
          <TableCell>
            <Button
              className="button"
              onClick={() => addTask(phrase, selectedDate)}
              variant="contained"
              color="primary"
            >
                        Add
            </Button>
          </TableCell>
        </MaterialTableRow>
      );
    }
}

TableSubHeader.propTypes = {
  addTask: PropTypes.func.isRequired,
  phrase: PropTypes.string.isRequired
};

export default connect(state => ({ phrase: state.inputValue.phrase }), { addTask })(TableSubHeader);
