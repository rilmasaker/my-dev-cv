import React from 'react';

import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

export default function TableHeader() {
  return (
    <TableRow className="table-header">
      <TableCell className="table-row">Content</TableCell>
      <TableCell className="table-row">Deadline</TableCell>
      <TableCell className="table-row">Action</TableCell>
    </TableRow>
  );
}
