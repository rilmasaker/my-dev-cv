/* eslint-disable no-shadow */
import React from 'react';
import { connect } from 'react-redux';
import Snackbar from '@material-ui/core/Snackbar';
import PropTypes from 'prop-types';

import { closeSnackBar } from '../actions/snackBar-action-creator';

function SnackbarInfo({ open, closeSnackBar, message = 'No more results' }) {
  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center'
      }}
      open={open}
      onClose={closeSnackBar}
      autoHideDuration={3000}
      message={message}
    />
  );
}

SnackbarInfo.propTypes = {
  open: PropTypes.bool.isRequired,
  message: PropTypes.string,
  closeSnackBar: PropTypes.func.isRequired
};

export default connect(state => state.snackBar, { closeSnackBar })(SnackbarInfo);
