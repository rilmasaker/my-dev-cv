/* eslint-disable no-shadow */
import React from 'react';
import { connect } from 'react-redux';
import TableCell from '@material-ui/core/TableCell';
import MaterialTableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';
import Delete from '@material-ui/icons/Delete';
import PropTypes from 'prop-types';

import { toggleTask, destroyTask } from '../actions/todo-action';

function TableRow({
  content,
  completed,
  deadline,
  id,
  toggleTask,
  destroyTask
}) {
  const isDone = completed
    ? 'isDone'
    : null;
  return (
    <MaterialTableRow>
      <TableCell>
        <span className={isDone}>{content}</span>
      </TableCell>
      <TableCell>{deadline}</TableCell>
      <TableCell style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Checkbox
          checked={completed}
          color="primary"
          onClick={() => toggleTask(completed, id)}
        />
        <Delete onClick={() => destroyTask(id)} />
      </TableCell>
    </MaterialTableRow>
  );
}

TableRow.propTypes = {
  content: PropTypes.string.isRequired,
  deadline: PropTypes.string.isRequired,
  toggleTask: PropTypes.func.isRequired,
  destroyTask: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
  completed: PropTypes.bool.isRequired
};

export default connect(null, { toggleTask, destroyTask })(TableRow);
