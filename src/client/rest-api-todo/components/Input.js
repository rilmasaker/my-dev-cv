/* eslint-disable no-shadow */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';

import { setPhrase } from '../actions/input-action-creators';

class Input extends Component {
    handleChange = (e) => {
      const { setPhrase } = this.props;
      setPhrase(e.target.value);
    }

    render() {
      const { phrase } = this.props;
      return (
        <div className="">
          <TextField
            required
            id="standard-required"
            placeholder="Add some text (required)"
            onChange={this.handleChange}
            value={phrase}
          />
        </div>
      );
    }
}

Input.propTypes = {
  phrase: PropTypes.string.isRequired,
  setPhrase: PropTypes.func.isRequired
};

export default connect(state => state.inputValue, { setPhrase })(Input);
