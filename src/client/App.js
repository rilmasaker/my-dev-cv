import React from 'react';
import Routes from './routes/Routes';
import NavBar2 from './components/NavBar2';
import Footer from './components/Footer';


export default function App() {
  return (
    <div className="app">
      <NavBar2 />
      <Routes />
      <Footer />
    </div>
  );
}
