/* eslint-disable no-shadow */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import myFireBase from './config';
import Logged from './Logged';
import LoginForm from './LoginForm';
import { unSetUser, setUser } from './actions/firebase-actions-creators';

class Login extends Component {
  componentDidMount() {
    this.authListener();
  }

  authListener= () => {
    const { unSetUser, setUser } = this.props;
    myFireBase.auth().onAuthStateChanged((user) => {
      if (user) {
        setUser({ user });
      } else {
        unSetUser();
      }
    });
  }

  render() {
    const { user } = this.props;
    return (
      <div>{user ? (<Logged />) : (<LoginForm />)}</div>
    );
  }
}

Login.propTypes = {
  unSetUser: PropTypes.func.isRequired,
  setUser: PropTypes.func.isRequired,
  user: PropTypes.any
};

export default connect(state => ({ user: state.firebase }), { unSetUser, setUser })(Login);
