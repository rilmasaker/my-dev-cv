import React from 'react';
import Button from '@material-ui/core/Button';

import myFireBase from './config';

export default function Logged() {
  const logout = () => {
    myFireBase.auth().signOut();
  };

  return (
    <div className="logout-form">
      <h1 className="login-header">Welcome</h1>
      <Button
        type="submit"
        className="button login-button"
        onClick={logout}
      >
           Logout
      </Button>
    </div>

  );
}
