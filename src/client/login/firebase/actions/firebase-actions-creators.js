import { ACTIONS } from '../reducers/firebase-reducer';

export function setUser(user) {
  return { type: ACTIONS.SET, user };
}

export function unSetUser() {
  return { type: ACTIONS.UNSET };
}
