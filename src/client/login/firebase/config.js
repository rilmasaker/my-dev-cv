import firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyA1PFMjPIMH86FlZbC5XpVl_K1_K4krDEY',
  authDomain: 'auth-marcin.firebaseapp.com',
  databaseURL: 'https://auth-marcin.firebaseio.com',
  projectId: 'auth-marcin',
  storageBucket: 'auth-marcin.appspot.com',
  messagingSenderId: '574141875214'
};

const myFireBase = firebase.initializeApp(config);
export default myFireBase;
