export const ACTIONS = {
  SET: Symbol('SET_USER'),
  UNSET: Symbol('UNSET_USER'),
};

export default function firebaseReducer(state = null, { type, user }) {
  switch (type) {
    case ACTIONS.SET:
      return [user];
    case ACTIONS.UNSET:
      return null;
    default:
      return state;
  }
}
