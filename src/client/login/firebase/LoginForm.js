/* eslint-disable no-shadow */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';

import myFireBase from './config';
import SnackBarInfo from '../../rest-api-todo/components/SnackBarInfo';
import { openSnackBar } from '../../rest-api-todo/actions/snackBar-action-creator';

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  login = (e) => {
    const { email, password } = this.state;
    const { openSnackBar } = this.props;
    e.preventDefault();
    myFireBase.auth().signInWithEmailAndPassword(email, password)
      .catch((error) => {
        openSnackBar(error.message);
      });
  }

  signup = (e) => {
    const { email, password } = this.state;
    const { openSnackBar } = this.props;
    e.preventDefault();
    myFireBase.auth().createUserWithEmailAndPassword(email, password)
      .then((u) => { console.log(u); })
      .catch((error) => {
        openSnackBar(error.message);
      });
  }

  render() {
    const { email, password } = this.state;
    return (
      <div className="login-form">
        <div className="form-group">
          <h1 className="login-header">LOGIN</h1>
          <input
            className="login-input"
            value={email}
            onChange={this.handleChange}
            type="email"
            name="email"
            id="email"
            placeholder="admin@mytest.org"
          />
          <input
            className="login-input"
            value={password}
            onChange={this.handleChange}
            type="password"
            name="password"
            id="password"
            placeholder="admin1"
          />
          <Button
            type="submit"
            className="button cv-button"
            onClick={this.login}
          >
            Login
          </Button>
          <Button
            type="submit"
            className="button login-button"
            onClick={this.signup}
          >
           Signup
          </Button>
        </div>
        <SnackBarInfo />
      </div>
    );
  }
}

LoginForm.propTypes = {
  openSnackBar: PropTypes.func.isRequired,
};


export default connect(null, { openSnackBar })(LoginForm);
