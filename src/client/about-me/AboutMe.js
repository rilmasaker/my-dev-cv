import React, { Fragment } from 'react';
import SkillList from './components/SkillsList';
import BoxesSkills from './components/BoxesSkills';
import Movie from './components/Movie';
import skillsList from './const';


export default function AboutMe() {
  return (
    <Fragment>
      <Movie />
      <div className="cv">
        <div className="cv-grey">
          <div className="cv-main">
            <img src="/static/me.jpg" title="Marcin" alt="My pictures" className="my-photo" />
            <div className="cv-address">
              <h3>Address</h3>
              <p>ul.Ksiazeca 18/9</p>
              <p>59-220 Legnica</p>
              <h3>Contact</h3>
              <p>+48518560087</p>
              <p>marcin_wydra@wp.pl</p>
              <h3>Date of Birth</h3>
              <p>14.04.1985</p>
            </div>
          </div>
          <div className="cv-skills">
            <SkillList list={skillsList.skill} title="SKILLS" />
            <SkillList list={skillsList.personality} title="PERSONALITY" />
            <BoxesSkills list={skillsList.software} title="SOFTWARESKILLS" />
            <BoxesSkills list={skillsList.languages} title="LANGUAGES" />
            <SkillList list={skillsList.hobbies} title="HOBBIES" />
          </div>
        </div>
        <div className="cv-white">
          <div className="cv-white-containter">
            <h1>MARCIN WYDRA</h1>
            <h2>EDUCATION</h2>
            <p>2008 - 2010</p>
            <strong><h4>University of Technology in Czestochowa</h4></strong>
            <p>MANAGEMENT (specialty: real estate management</p>
            <p>2005 - 2008</p>
            <strong><h4>State Vocational College in Nysa</h4></strong>
            <p>FINANCE AND BANKING (specialty: management)</p>
            <h2>EXPERIENCE</h2>
            <p>01.2018 - present</p>
            <strong><h4>JUNIOR FRONTEND DEVELOPER</h4></strong>
            <strong><h4>ASTEK Polska sp. z o.o</h4></strong>
            <p>
                  Creating code, implementation of software,
                  refactoring; assessment of complexity and impact on
                  the current or planned system; creating or selecting the
                  most appropriate methods, techniques, standards. tips
                  or tools for software development; testing of
                  alternative solutions and verification of their feasibility
            </p>
            <p>09.2015 - present</p>
            <strong><h4>OWNER OF A BERRY PLANTATION</h4></strong>
            <p>
                  I have set up and run a berry plantation with an area of
                  1 ha (about 3 thousand trees)
            </p>
            <p>08.2015 - 11.2016</p>
            <strong><h4>ROPE ACCESS TECHNICIAN</h4></strong>
            <p>
                I worked on ropes in many locations such as
                Volkswagen in Wrzesnia. They were temporary contract
                jobs)
            </p>
            <p>02.2015 - 08.2017</p>
            <strong><h4>OWNER OF AN ADVERTISING AGENCY</h4></strong>
            <p>
                I led an advertising agency running in the following
                areas: publication of an advertising paper with
                vouchers, sale of advertising in direct contact with the
                customer, promotion on the Internet)
            </p>
            <p>04.2009 - 01.2015</p>
            <strong><h4>TEXAS HOLDEM PROFESSIONAL POKER PLAYER</h4></strong>
            <p>
                During my career years I won many poker tournaments
                in Poland and abroad. I used to be an inofficial Poker
                Champion of Poland in poker duels and a poker trainer.
                I used to write articles for the bigger poker website in
                Poland.)
            </p>
            <strong><h4>VOLUNTEERING, UGANDA (AFRICA)</h4></strong>
            <p>
                Carrying out of a voluntary project in winter seasons:
                building a playground in Kyenjojo and a well in
                Niyakagongo. During the projects I was running a fanpage
                on Facebook @pobandziewugandzie
            </p>
          </div>
        </div>
      </div>
    </Fragment>
  );
}
