import { ACTIONS } from '../reducers/dialog-reducer';

export function openDialog(message) {
  return { type: ACTIONS.OPEN, message };
}

export function closeDialog() {
  return { type: ACTIONS.CLOSE };
}

export function openCVDialog() {
  return { type: ACTIONS.OPEN_CV };
}

export function closeCVDialog() {
  return { type: ACTIONS.CLOSE_CV };
}
