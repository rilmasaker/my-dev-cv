import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import Button from '@material-ui/core/Button';

import SkillList from './components/SkillsList';
import BoxesSkills from './components/BoxesSkills';
import Movie from './components/Movie';
import skillsList from './const';

export default function AboutMe() {
  return (
    <Fragment>
      <Movie />
      <div className="cv">
        <div className="cv-grey">
          <div className="cv-skills">
            <BoxesSkills list={skillsList.software} />
          </div>
        </div>
      </div>
      <div>
        <h1 className="skills-title">SKILLS</h1>
        <div className="skills-buttons-container">
          <NavLink className="skills-link" to="/form">
            <Button className="skills-button">
               FORM
            </Button>
          </NavLink>
          <NavLink className="skills-link" to="/rest-api">
            <Button className="skills-button">
               REST API
            </Button>
          </NavLink>
          <NavLink className="skills-link" to="/database">
            <Button className="skills-button">
               DATABASE
            </Button>
          </NavLink>
          <NavLink className="skills-link" to="/auth">
            <Button className="skills-button">
               LOGIN
            </Button>
          </NavLink>
        </div>
      </div>
      <div>

        
      </div>
    </Fragment>
  );
}
