const skillsList = {
  skill: [
    'I learn fast and effective',
    'Making decisions and taking responsibility for them',
    'Ability to self-organize my work',
    'Driving license category B'
  ],
  personality: [
    'Willing to develop',
    'Communicative',
    'Creativity',
  ],
  software: [
    ['JAVASCRIPT', 3, 'Loops, (map, filter, reduce, forEach), ES6 etc'],
    ['REACT', 3, 'HOC, React router, code organisation'],
    ['REDUX', 3, 'Actions, States and so on '],
    ['HTML', 4, 'In fact, more than HTML a know JSX :)'],
    ['CSS/LESS', 4, 'Preprocessors, Flex-box, responisive designs '],
    ['GIT, JIRA', 3, 'I dont know how it possible to work wihout that'],
    ['SQLITE', 3, 'Sequelise is usefull for me'],
  ],
  languages: [
    ['English', 4, 'Im working on it still']
  ],
  hobbies: [
    'Chess, Snowboard, Traveling, Cooking, Coding'
  ],
};
export const video = 'DKniZNZHSJs';

export default skillsList;
