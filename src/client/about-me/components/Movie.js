import React from 'react';
import YouTube from 'react-youtube';
import Typed from 'react-typed';

import { video } from '../const';


export default class Movie extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      videoId: video,
      hide: '',
      width: 640
    };
  }

  componentDidMount() {
    if (window.innerWidth < 750) {
      this.setState({ width: window.innerWidth - 40 });
    }

    if
    (window.innerWidth < 300) {
      this.setState({ width: window.innerWidth - 20 });
    }
  }

  render() {
    const { hide, videoId, width } = this.state;
    return (
      <div className={`you-tube ${hide}`}>
        <div className="typewriter-div">
          <p className="typewriter">
            <Typed
              strings={['Hello, I am Marcin Wydra. I am a developer.']}
              typeSpeed={200}
            />
          </p>
        </div>
        <div>
          <YouTube
            videoId={videoId}
            opts={
            {
              height: '390',
              width: `${width}`,
              playerVars: {
                autoplay: 1
              }
            }}
            onEnd={() => { this.setState({ hide: '' }); }}
          />
        </div>

      </div>
    );
  }
}
