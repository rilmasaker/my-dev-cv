/* eslint-disable no-shadow */
import React from 'react';
import { connect } from 'react-redux';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import Fab from '@material-ui/core/Fab';
import Close from '@material-ui/icons/Close';
import PropTypes from 'prop-types';

import { closeDialog } from '../actions/dialog-actions-creator';


function SkillsDialog({ open, text, closeDialog }) {
  return (
    <Dialog
      open={open}
      fullWidth
    >
      <DialogContent style={{
        display: 'flex', alignItems: 'center', justifyContent: 'space-evenly', background: '#202020', color: '#33b600'
      }}
      >
        <p style={{ fontSize: '24px' }}>{text}</p>
        <Fab onClick={closeDialog} style={{ backgroundColor: '#313131', color: '#33b600' }}>
          <Close style={{}} />
        </Fab>
      </DialogContent>
    </Dialog>
  );
}

SkillsDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
  closeDialog: PropTypes.func.isRequired,
};


export default connect(state => ({ open: state.dialog.open }), { closeDialog })(SkillsDialog);
