import React, { Fragment } from 'react';
import Done from '@material-ui/icons/Done';
import PropTypes from 'prop-types';

export default function SkillList({ list, title }) {
  return (
    <Fragment>
      <h3 className="cv-skills-title">{title}</h3>
      <ul className="cv-skills-ul">
        {list.map(rowText => (
          <li key={rowText}>
            <Done className="cv-skills-done" />
            {rowText}
          </li>
        ))}
      </ul>
    </Fragment>
  );
}

SkillList.propTypes = {
  list: PropTypes.array.isRequired,
  title: PropTypes.string.isRequired,
};
