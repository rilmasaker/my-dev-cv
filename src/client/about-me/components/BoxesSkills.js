/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { openDialog } from '../actions/dialog-actions-creator';
import SkillsDialog from './SkillsDialog';


function BoxesSkills({
  // eslint-disable-next-line no-shadow
  list, title, openDialog, message
}) {
  const whiteDiv = <div className="cv-white-box" />;
  const greyDiv = <div className="cv-grey-box" />;
  function Boxes({ num }) {
    const box = [];
    for (let i = 0; i < 6; i += 1) {
      const myDiv = (i < num) ? whiteDiv : greyDiv;
      box.push(
        <div className="cv-box" key={i}>{myDiv}</div>
      );
    }
    return box;
  }

  BoxesSkills.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    list: PropTypes.array.isRequired,
    title: PropTypes.string.isRequired,
    openDialog: PropTypes.func.isRequired,
    message: PropTypes.string.isRequired
  };

  function Skills() {
    return (
      <Fragment>
        <SkillsDialog text={message} />
        {list.map((skill) => {
          const [name, num, text] = skill;
          return (
            // eslint-disable-next-line jsx-a11y/click-events-have-key-events
            <li key={name} className="cv-software" onClick={() => openDialog(text)} style={{ cursor: 'pointer' }}>
              {name}
              <div className="cv-boxes">
                <Boxes num={num} />
              </div>
            </li>
          );
        })}
      </Fragment>
    );
  }

  return (
    <Fragment>
      <h3 className="cv-skills-title">{title}</h3>
      <ul className="cv-skills-ul">
        <Skills />
      </ul>
    </Fragment>
  );
}

export default connect(state => ({ message: state.dialog.message }), { openDialog })(BoxesSkills);
