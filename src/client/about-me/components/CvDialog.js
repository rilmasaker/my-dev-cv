import React from 'react';
import { connect } from 'react-redux';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import Fab from '@material-ui/core/Fab';
import Close from '@material-ui/icons/Close';
import PropTypes from 'prop-types';

import { closeCVDialog } from '../actions/dialog-actions-creator';


// eslint-disable-next-line no-shadow
function CvDialog({ open, closeCVDialog }) {
  return (
    <Dialog
      open={open}
      fullWidth
    >
      <DialogContent style={{
        display: 'flex', alignItems: 'center',
      }}
      >
        <div className="cv-white">
          <div className="cv-white-containter">
            <h1>MARCIN WYDRA</h1>
            <h2>EDUCATION</h2>
            <p>2008 - 2010</p>
            <strong><h4>University of Technology in Czestochowa</h4></strong>
            <p>MANAGEMENT (specialty: real estate management</p>
            <p>2005 - 2008</p>
            <strong><h4>State Vocational College in Nysa</h4></strong>
            <p>FINANCE AND BANKING (specialty: management)</p>
            <h2>EXPERIENCE</h2>
            <p>01.2018 - present</p>
            <strong><h4>JUNIOR FRONTEND DEVELOPER</h4></strong>
            <strong><h4>ASTEK Polska sp. z o.o</h4></strong>
            <p>
                  Creating code, implementation of software,
                  refactoring; assessment of complexity and impact on
                  the current or planned system; creating or selecting the
                  most appropriate methods, techniques, standards. tips
                  or tools for software development; testing of
                  alternative solutions and verification of their feasibility
            </p>
            <p>09.2015 - present</p>
            <strong><h4>OWNER OF A BERRY PLANTATION</h4></strong>
            <p>
                  I have set up and run a berry plantation with an area of
                  1 ha (about 3 thousand trees)
            </p>
            <p>08.2015 - 11.2016</p>
            <strong><h4>ROPE ACCESS TECHNICIAN</h4></strong>
            <p>
                I worked on ropes in many locations such as
                Volkswagen in Wrzesnia. They were temporary contract
                jobs)
            </p>
            <p>02.2015 - 08.2017</p>
            <strong><h4>OWNER OF AN ADVERTISING AGENCY</h4></strong>
            <p>
                I led an advertising agency running in the following
                areas: publication of an advertising paper with
                vouchers, sale of advertising in direct contact with the
                customer, promotion on the Internet)
            </p>
            <p>04.2009 - 01.2015</p>
            <strong><h4>TEXAS HOLDEM PROFESSIONAL POKER PLAYER</h4></strong>
            <p>
                During my career years I won many poker tournaments
                in Poland and abroad. I used to be an inofficial Poker
                Champion of Poland in poker duels and a poker trainer.
                I used to write articles for the bigger poker website in
                Poland.)
            </p>
            <strong><h4>VOLUNTEERING, UGANDA (AFRICA)</h4></strong>
            <p>
                Carrying out of a voluntary project in winter seasons:
                building a playground in Kyenjojo and a well in
                Niyakagongo. During the projects I was running a fanpage
                on Facebook @pobandziewugandzie
            </p>
          </div>
        </div>
        <Fab onClick={closeCVDialog} style={{ backgroundColor: '#313131', color: '#33b600' }}>
          <Close style={{}} />
        </Fab>
      </DialogContent>
    </Dialog>
  );
}

CvDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  closeCVDialog: PropTypes.func.isRequired,
};


// eslint-disable-next-line max-len
export default connect(state => ({ open: state.cvDialogReducer.open }), { closeCVDialog })(CvDialog);
