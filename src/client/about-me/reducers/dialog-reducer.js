export const ACTIONS = {
  OPEN: Symbol('OPEN_DIALOG'),
  CLOSE: Symbol('CLOSE_DIALOG'),
  OPEN_CV: Symbol('OPEN_CV_DIALOG'),
  CLOSE_CV: Symbol('CLOSE_CV_DIALOG')
};

export default function dialogReducer(state = { open: false, message: '' }, { type, message }) {
  switch (type) {
    case ACTIONS.OPEN:
      return {
        ...state,
        open: true,
        message
      };

    case ACTIONS.CLOSE:
      return {
        ...state,
        open: false,
      };

    default:
      return state;
  }
}

export function cvDialogReducer(state = { open: false }, { type }) {
  switch (type) {
    case ACTIONS.OPEN_CV:
      return {
        ...state,
        open: true,
      };
    case ACTIONS.CLOSE_CV:
      return {
        ...state,
        open: false,
      };

    default:
      return state;
  }
}
