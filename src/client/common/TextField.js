import MuTextField from '@material-ui/core/TextField';
import React from 'react';
import PropTypes from 'prop-types';

export default function TextField({
  input,
  labelText,
  meta: {
    touched,
    error = {}
  },
  ...custom
}) {
  const hasError = touched && error.constructor !== Object;

  return (
    <MuTextField
      error={hasError}
      helperText={hasError
        ? error
        : ''}
      {...input}
      {...custom}
    />
  );
}

TextField.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  labelText: PropTypes.string,
};
