import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import PropTypes from 'prop-types';

export default function CheckBoxes({
  input,
  meta: {
    touched,
    error = false
  },
  ...custom
}) {
  return (
    <FormGroup row>
      <FormControlLabel
        control={(
          <Checkbox
            checked={
                input.value
            }
            onChange={
                event => input.onChange(event.target.checked)
            }
            color="primary"
          />
)}
        {...custom}
      />
      {touched && error && (
        <FormHelperText style={{ color: 'red' }}>
          {error}
        </FormHelperText>
      )
}
    </FormGroup>
  );
}

CheckBoxes.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
};
