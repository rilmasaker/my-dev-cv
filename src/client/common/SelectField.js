/* eslint-disable no-unused-vars */
import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import PropTypes from 'prop-types';

export default function SelectField({
  input,
  label,
  meta: {
    touched,
    error
  },
  children,
  ...custom
}) {
  const [state, setState] = React.useState({ languages: '', labelWidth: 0 });
  const inputLabelRef = React.useRef(null);

  function handleChange(event) {
    setState({
      ...state,
      [event.target.name]: event.target.value
    });
  }

  return (
    <FormControl
      variant="outlined"
      style={{ width: '100%' }}
    >
      <InputLabel ref={inputLabelRef} htmlFor="outlined-Languages-simple">Languages</InputLabel>
      <Select
        {...input}
        {...custom}
        className="test"
        value={state.languages}
        onChange={handleChange}
        input={(
          <OutlinedInput
            labelWidth={state.labelWidth}
            name="languages"
            id="outlined-Languages-simple"
          />
)}
        inputProps={{
          name: 'languages',
          id: 'Languages-simple'
        }}
      >
        {children}
      </Select>
    </FormControl>
  );
}

SelectField.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired
};
