/* eslint-disable react/no-array-index-key */
import React from 'react';
import MaterialTable from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import PropTypes from 'prop-types';

export default function Table({
  row: Row,
  header: Header,
  rowProps = {},
  subHeader: SubHeader = () => {},
  tableProps = {},
  bodyProps = {},
  headerProps = {},
  list = [],
  style = {},
  className = ''
}) {
  return (
    <section className={className}>
      <MaterialTable style={style} {...tableProps}>
        <TableHead {...headerProps}>
          <Header />
        </TableHead>
        <TableBody {...bodyProps}>
          <SubHeader />
          {list.map((rowData, index) => <Row key={`${index} ${rowData}`} {...rowData} {...rowProps} />)}
        </TableBody>
      </MaterialTable>
    </section>
  );
}

Table.propTypes = {
  header: PropTypes.func.isRequired,
  rowProps: PropTypes.object,
  subHeader: PropTypes.func.isRequired,
  bodyProps: PropTypes.object,
  headerProps: PropTypes.object,
  list: PropTypes.array.isRequired,
  style: PropTypes.object,
  row: PropTypes.func.isRequired,
  tableProps: PropTypes.object,
  className: PropTypes.string
};
