export const ACTIONS = {
  TOGGLE: Symbol('TOGGLE_EXP')
};

export default function skipExpReducer(state = true, { type }) {
  switch (type) {
    case ACTIONS.TOGGLE:
      return !state;
    default:
      return state;
  }
}
