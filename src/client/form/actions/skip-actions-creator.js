import { ACTIONS } from '../reducers/skipExp-reducer';

export default function skipExp() {
  return { type: ACTIONS.TOGGLE };
}
