import React from 'react';
import AppBar from '@material-ui/core/AppBar';

export default function Header() {
  return (
    <AppBar
      position="static"
      style={{
        height: '100px',
        marginBottom: '20px'
      }}
    >
            Join Us
    </AppBar>
  );
}
