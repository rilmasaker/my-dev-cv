import { Field } from 'redux-form';
import React from 'react';

import TextField from '../../common/TextField';

export default function Contact() {
  return (
    <div
      className="section"
    >
      <p className="title">Contact</p>
      <Field
        name="Phone"
        className="s-text s-name"
        placeholder="Phone"
        variant="outlined"
        component={TextField}
      />
      <Field
        name="email"
        className="s-text"
        placeholder="email"
        variant="outlined"
        component={TextField}
      />
    </div>
  );
}
