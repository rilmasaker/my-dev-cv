import { Field } from 'redux-form';
import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import TextField from '../../common/TextField';


function Experience({ toggleExp }) {
  return (
    <Fragment>
      {toggleExp && (
      <div className="section">
        <p className="title">Experience</p>
        <Field
          name="Current Position"
          className="s-text s-name"
          placeholder="Current position"
          variant="outlined"
          component={TextField}
        />
        <Field
          name="Technologies"
          className="s-text"
          placeholder="Technologies"
          variant="outlined"
          component={TextField}
        />
      </div>
      )
}
    </Fragment>
  );
}

Experience.propTypes = {
  toggleExp: PropTypes.bool.isRequired,
};

export default connect(state => ({ toggleExp: state.skipExp }))(Experience);
