import React from 'react';
import { Field } from 'redux-form';
import TextField from '../../common/TextField';

export default function MainInfo() {
  return (
    <div className="section">
      <p className="title">Main Information</p>
      <Field
        className="s-name s-text"
        placeholder="Name"
        variant="outlined"
        component={TextField}
        name="Name"
      />
      <Field
        name="Surname"
        className="s-text"
        placeholder="Surname"
        variant="outlined"
        component={TextField}
      />
    </div>
  );
}
