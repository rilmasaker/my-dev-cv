/* eslint-disable react/no-array-index-key */
import React from 'react';
import { Field, FieldArray } from 'redux-form';
import MenuItem from '@material-ui/core/MenuItem';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Remove from '@material-ui/icons/Remove';
import PropTypes from 'prop-types';

import SelectField from '../../common/SelectField';

function Language({ member }) {
  return (

    <Field name={`${member}.language`} component={SelectField} label="Languages">
      <MenuItem value="english">English</MenuItem>
      <MenuItem value="german">German</MenuItem>
      <MenuItem value="spanish">Spanish</MenuItem>
      <MenuItem value="russian">Russian</MenuItem>
    </Field>
  );
}
Language.propTypes = {
  member: PropTypes.string.isRequired,
};

function LanguageArray({ fields }) {
  return (
    <div className="section">
      <p className="title">Languages</p>
      {fields.map((member, index) => (
        <div key={index}>
          <Language member={member} />
          {fields.length > 1
                        && (
                        <div className="remove-button">
                          <Fab color="primary" aria-label="Remove" onClick={() => fields.remove(index)}>
                            <Remove />
                          </Fab>
                        </div>
                        )}
        </div>
      ))}
      {fields.length < 3
                 && (
                 <div className="add-button">
                   <Fab color="primary" aria-label="Add" onClick={() => fields.push()}>
                     <AddIcon />
                   </Fab>
                 </div>
                 )
           }
    </div>
  );
}

LanguageArray.propTypes = {
  fields: PropTypes.object.isRequired,
};

export default function Languages() {
  return (
    <FieldArray name="languages" component={LanguageArray} />
  );
}
