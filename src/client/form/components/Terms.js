import React from 'react';

import { Field } from 'redux-form';
import CheckBoxes from '../../common/CheckBox';

export default function Terms() {
  return (
    <div className="section terms">
      <Field
        name="Terms"
        component={CheckBoxes}
        label="I agree with you :)"
      />
    </div>
  );
}
