import { Field } from 'redux-form';
import React from 'react';

import TextField from '../../common/TextField';

export default function () {
  return (
    <div
      className="section"
    >
      <p className="title">Adress</p>
      <Field
        name="Adress"
        className="s-text s-name"
        placeholder="Adress"
        variant="outlined"
        component={TextField}
      />
      <Field
        name="City"
        className="s-text"
        placeholder="City"
        variant="outlined"
        component={TextField}
      />
    </div>
  );
}
