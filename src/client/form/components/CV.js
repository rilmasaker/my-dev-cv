/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import { Field } from 'redux-form';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';

const FileInput = ({ input, resetKey }) => {
  const {
    value,
    ...inputProps
  } = input;

  const handleChange = (e) => {
    input.onChange(e.target.files[0]);
  };

  return (
    <input
      {...inputProps}
      key={resetKey}
      id="contained-button-file"
      style={{ display: 'none' }}
      type="file"
      onChange={handleChange}
      onBlur={() => {}}
    />
  );
};

FileInput.propTypes = {
  input: PropTypes.object.isRequired,
  resetKey: PropTypes.string
};


export default function Cv() {
  return (
    <div className="section">
      <p className="title">Curiculum Vitae</p>
      <label htmlFor="contained-button-file">
        <Button
          className="button button-form"
          variant="contained"
          component="span"
          color="primary"
          style={{
            width: '100%'
          }}
        >
            Upload CV
          <Field type="file" name="CV" component={FileInput} />
        </Button>
      </label>
    </div>
  );
}
