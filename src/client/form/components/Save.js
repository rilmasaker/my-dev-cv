import React from 'react';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';

export default function Save({ onClick }) {
  return (
    <div className="section">
      <Button
        className="button button-form"
        variant="contained"
        type="submit"
        color="primary"
        style={{
          width: '100%',
          marginBottom: '20px'
        }}
      >
                Save
      </Button>
      <Button
        className="button button-form"
        variant="contained"
        type="submit"
        color="primary"
        style={{
          width: '100%'
        }}
        onClick={onClick}
      >
                Clear
      </Button>
    </div>
  );
}

Save.propTypes = {
  onClick: PropTypes.func.isRequired,
};
