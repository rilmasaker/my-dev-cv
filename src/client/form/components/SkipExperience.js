/* eslint-disable no-shadow */
import React from 'react';
import { connect } from 'react-redux';
import Switch from '@material-ui/core/Switch';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import PropTypes from 'prop-types';

import skipExp from '../actions/skip-actions-creator';


function SkipExperience({ skipExp }) {
  const [state, setState] = React.useState({
    checkedA: false,
  });

  const handleChange = name => (event) => {
    setState({ ...state, [name]: event.target.checked });
    skipExp();
  };

  return (
    <div className="section" style={{ marginTop: '20px' }}>
      <FormGroup row>
        <FormControlLabel
          control={(
            <Switch
              checked={state.checkedA}
              onChange={handleChange('checkedA')}
              value="checkedA"
              color="primary"
            />
)}
          label="Skip experience details"
        />
      </FormGroup>
    </div>
  );
}

SkipExperience.propTypes = {
  skipExp: PropTypes.func.isRequired,
};

export default connect(null, { skipExp })(SkipExperience);
