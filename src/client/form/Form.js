import React, { Component } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';

import { reduxForm } from 'redux-form';
import MainIfno from './components/MainInfo';
import Contact from './components/Contact';
import Languages from './components/Languages';
import Adress from './components/Adress';
import Experience from './components/Experience';
import SkipExperience from './components/SkipExperience';
import Cv from './components/CV';
import Terms from './components/Terms';
import Save from './components/Save';
import validate from '../utils/validate';


axios.create({
  baseURL: 'http://localhost:8080/'
});

class Form extends Component {
    submit = (e) => {
      const { handleSubmit } = this.props;
      e.preventDefault();
      handleSubmit((values) => {
        const user = {
          name: values.Name,
          surname: values.Surname,
          email: values.email
        };
        axios.post('/api/user/create', user);
      })();
    }

    render() {
      const { reset } = this.props;
      return (
        <div className="container">
          <form
            onSubmit={this.submit}
            style={{
              display: 'flex',
              justifyContent: 'center'
            }}
          >
            <div className="paper">
              <MainIfno />
              <Contact />
              <Adress />
              <Languages />
              <SkipExperience />
              <Experience />
              <Cv />
              <Terms />
              <Save onClick={reset} />
            </div>
          </form>
        </div>
      );
    }
}

Form.propTypes = {
  reset: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({
  form: 'contactForm',
  validate,
  initialValues: {
    languages: [{}]
  }
})(Form);
