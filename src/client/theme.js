import { createMuiTheme } from '@material-ui/core';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#33b600',
    }
  },
  overrides: {
    MuiInput: {
      underline: {
        borderBottom: '2px solid #33b600',
        color: '#33b600',
        fontSize: '1vw',
        '&:after': {
          borderBottom: 'none',
        },
      }
    },
    MuiTypography: {
      body1: {
        color: '#e5e5e5',
        fontSize: '1vw'
      }
    },

    MuiFab: {
      root: {
        width: '3vw',
        height: '3vw',
        minHeight: '2vw',
      }
    },
    MuiSvgIcon: {
      root: {
        cursor: 'pointer'
      }
    },

    MuiCheckbox: {
      root: {
        color: '#33b600'
      }
    },


    MuiFilledInput: {
      underline: {
        borderBottom: '2px solid #33b600',
        color: '#33b600',
        fontSize: '2vw',
        '&:after': {
          borderBottom: 'none',
        },
      }
    },
    MuiOutlinedInput: {
      root: {
        backgroundColor: '#e5e5e5',
        borderRadius: 0,
      },
      notchedOutline: {
        border: '1px solid #33b600',
      },
      input: {
        padding: '5px 10px 5px 0',
        fontSize: '1.5vw',
        lineHeight: '23px',
        marginLeft: '20px'
      },
    },
  }
});

export default theme;
