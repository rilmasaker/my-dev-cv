/* eslint-disable import/named */
import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { reducer as formReducer } from 'redux-form';

import skipExp from './form/reducers/skipExp-reducer';
import toDo from './rest-api-todo/reducers/toDo-reducer';
import inputValue from './rest-api-todo/reducers/input-reducer';
import snackBar from './rest-api-todo/reducers/snackBar-reducer';
import users from './database-example/reducers/database-reducer';
import dialog, { cvDialogReducer } from './about-me/reducers/dialog-reducer';
import firebase from './login/firebase/reducers/firebase-reducer';


const store = createStore(combineReducers({
  skipExp,
  toDo,
  inputValue,
  snackBar,
  users,
  dialog,
  cvDialogReducer,
  firebase,
  form: formReducer

}), {}, applyMiddleware(thunk));

export default store;
