import React, { Fragment, Component } from 'react';
import { NavLink } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';


export default class NavBar extends Component {
  state = {
    anchorEl: null,
    width: null
  };

  componentWillMount() {
    this.updateDimensions();
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }

  handleClick = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  updateDimensions=() => {
    this.setState({ width: window.innerWidth - 10 });
  }

  render() {
    const { anchorEl, width } = this.state;
    return (
      <Fragment>
        {width < 640
        && (
        <div className="nav-bar">
          <Button
            className="button"
            variant="contained"
            aria-owns={anchorEl ? 'simple-menu' : undefined}
            aria-haspopup="true"
            onClick={this.handleClick}
          >
          Open Menu
          </Button>
          <Menu

            id="simple-menu"
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={this.handleClose}
          >
            <MenuItem
              onClick={this.handleClose}
              className="menu-item"
            >
              <NavLink className="menu-link" to="/">
               ABOUT ME
              </NavLink>
            </MenuItem>
            <MenuItem onClick={this.handleClose} className="menu-item">
              <NavLink className="menu-link" to="/form">
               REDUX FORM
              </NavLink>
            </MenuItem>
            <MenuItem onClick={this.handleClose} className="menu-item">
              <NavLink className="menu-link" to="/rest-api">
               REST API
              </NavLink>
            </MenuItem>
            <MenuItem onClick={this.handleClose} className="menu-item">
              <NavLink className="menu-link" to="/database">
               DATABASE
              </NavLink>
            </MenuItem>
            <MenuItem onClick={this.handleClose} className="menu-item">
              <NavLink className="menu-link" to="/auth">
               AUTH
              </NavLink>

            </MenuItem>
          </Menu>
        </div>
        )
          }
        {width > 640
           && (
           <div className="nav-bar">
             <NavLink to="/">
               <Button
                 className="button"
                 variant="contained"
               >
               ABOUT ME
               </Button>
             </NavLink>
             <NavLink to="/form">
               <Button
                 className="button"
                 variant="contained"
               >
               REDUX FORM
               </Button>
             </NavLink>
             <NavLink to="/rest-api">
               <Button
                 className="button"
                 variant="contained"
               >
               REST API
               </Button>
             </NavLink>
             <NavLink to="/database">
               <Button
                 className="button"
                 variant="contained"
               >
               DATABASE
               </Button>
             </NavLink>
             <NavLink to="/auth">
               <Button
                 className="button"
                 variant="contained"
               >
               AUTH
               </Button>
             </NavLink>
           </div>
           )}

      </Fragment>
    );
  }
}
