/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { Fragment, Component } from 'react';
import { NavLink } from 'react-router-dom';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';


export default class NavBar extends Component {
    state = {
      open: false,
    };

      handleClick = () => {
        this.setState(state => ({ open: !state.open }));
      };

      handleClose = (event) => {
        if (this.anchorEl.contains(event.target)) {
          return;
        }

        this.setState({ open: false });
      };

      render() {
        const { open } = this.state;
        return (
          <Fragment>
            <div className="nav">
              <NavLink className="nav-link2" to="/">
               ABOUT ME
                <i className="fas fa-hand-point-left" />
              </NavLink>
              <div className="">
                <p
                  className="nav-link2"
                  to="/"
                  aria-owns={open ? 'menu-list-grow' : undefined}
                  aria-haspopup="true"
                  ref={(node) => {
                    this.anchorEl = node;
                  }}
                  onClick={this.handleClick}
                >
                  <i className="fas fa-hand-point-right" />
                    MY SKILLS
                </p>

                <Popper open={open} anchorEl={this.anchorEl} transition disablePortal style={{ zIndex: '1' }}>
                  {({ TransitionProps, placement }) => (
                    <Grow
                      {...TransitionProps}
                      id="menu-list-grow"
                      style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                    >
                      <Paper>
                        <ClickAwayListener onClickAway={this.handleClose}>
                          <MenuList className="nav-menu">
                            <MenuItem className="nav-menu-item" onClick={this.handleClose}>
                              <NavLink className="nav-link2" to="/form">FORM</NavLink>
                            </MenuItem>
                            <MenuItem className="nav-menu-item" onClick={this.handleClose}>
                              <NavLink className="nav-link2" to="/rest-api">REST API</NavLink>
                            </MenuItem>
                            <MenuItem className="nav-menu-item" onClick={this.handleClose}>
                              <NavLink className="nav-link2" to="/database">DATABASE</NavLink>
                            </MenuItem>
                            <MenuItem className="nav-menu-item" onClick={this.handleClose}>
                              <NavLink className="nav-link2" to="/auth">LOGIN</NavLink>
                            </MenuItem>
                          </MenuList>
                        </ClickAwayListener>
                      </Paper>
                    </Grow>
                  )}
                </Popper>
              </div>
            </div>

          </Fragment>
        );
      }
}
