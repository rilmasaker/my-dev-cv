/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-shadow */
import React, { Fragment, Component } from 'react';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';

import { openCVDialog } from '../about-me/actions/dialog-actions-creator';
import CvDialog from '../about-me/components/CvDialog';


class Footer extends Component {
    state = {
      name: '',
      email: '',
      multiline: '',
    };

      handleChange = name => (event) => {
        this.setState({ [name]: event.target.value });
      };

      render() {
        const { email, name, multiline } = this.state;
        const { openCVDialog } = this.props;
        return (
          <Fragment>
            <div className="footer">
              <div className="contact-form">
                <TextField
                  id="standard-name"
                  label="Enter your name"
                  className="text-field"
                  value={name}
                  onChange={this.handleChange('name')}
                />
                <TextField
                  id="standard-email"
                  label="Enter your email adress"
                  value={email}
                  className="text-field"
                  onChange={this.handleChange('email')}
                />
                <TextField
                  id="standard-multiline-flexible"
                  label="Enter your message"
                  className="text-field"
                  style={{ marginTop: '13vw' }}
                  multiline
                  rowsMax="10"
                  value={multiline}
                  onChange={this.handleChange('multiline')}
                />
                <Button type="submit" className="button cv-button"> Submit</Button>

              </div>

              <div className="cv-right">
                <div className="cv-main">
                  <img src="/static/me.jpg" title="Marcin" alt="My pictures" className="my-photo" />
                  <div className="cv-address">
                    <h3>Marcin Wydra</h3>
                    <p>I learn fast and effective</p>
                    <p>I can making decisions and taking responsibility for them</p>
                    <p>I have ability to self-organize my work</p>
                  </div>
                </div>
                <h1 className="cv-big" onClick={openCVDialog}>CURICULUM VITEA</h1>
                <p className="cv-small">Contact me: +48 518 560 087</p>
                <p className="cv-small">Email me: marcin_wydra@wp.pl</p>
              </div>
              <CvDialog />
            </div>
            <div className="copy-right">
              <p className="copy-right-paragraph">Copyright &#174; 2019 Marcin Wydra All rights reserved</p>
            </div>
          </Fragment>
        );
      }
}

Footer.propTypes = {
  openCVDialog: PropTypes.func.isRequired,
};

export default connect(null, { openCVDialog })(Footer);
