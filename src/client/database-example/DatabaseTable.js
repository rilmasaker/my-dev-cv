/* eslint-disable no-shadow */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';

import Table from '../common/Table';
import TableRow from './components/TableRow';
import TableHeader from './components/TableHeader';
import { get } from '../utils/http';
import { setUsers } from './actions/database-actions-creators';


class DatabaseTable extends Component {
  componentDidMount() {
    const { setUsers } = this.props;
    get('/api/getDatabase')
      .then((users) => {
        setUsers(users);
      });
  }

  render() {
    const { list } = this.props;
    return (
      <div style={{
        marginTop: '5%',
        marginLeft: '10%',
        marginRight: '10%',
      }}
      >
        <Table
          row={TableRow}
          header={TableHeader}
          subHeader={() => null}
          list={list}
        />
        <NavLink to="/form">
          <Button
            className="button database-button"
            variant="contained"
          >
          ADD TO DATABASE
          </Button>
        </NavLink>
      </div>
    );
  }
}

DatabaseTable.propTypes = {
  list: PropTypes.array.isRequired,
  setUsers: PropTypes.func.isRequired
};

export default connect(state => ({ list: state.users }), { setUsers })(DatabaseTable);
