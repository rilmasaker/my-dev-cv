/* eslint-disable no-shadow */
import React from 'react';
import { connect } from 'react-redux';
import TableCell from '@material-ui/core/TableCell';
import MaterialTableRow from '@material-ui/core/TableRow';
import Delete from '@material-ui/icons/Delete';
import PropTypes from 'prop-types';

import destroyUser from '../actions/database-actions';


function TableRow({
  name = '', surname = '', email = '', id, destroyUser
}) {
  return (
    <MaterialTableRow>
      <TableCell>{name}</TableCell>
      <TableCell>{surname}</TableCell>
      <TableCell>{email}</TableCell>
      <TableCell><Delete onClick={() => destroyUser(id)} /></TableCell>
    </MaterialTableRow>
  );
}

TableRow.propTypes = {
  surname: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  destroyUser: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired
};

export default connect(null, { destroyUser })(TableRow);
