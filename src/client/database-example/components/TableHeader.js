import React from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

export default function TableHeader() {
  return (
    <TableRow className="table-header">
      <TableCell className="table-row">Name</TableCell>
      <TableCell className="table-row">Surname</TableCell>
      <TableCell className="table-row">Email</TableCell>
      <TableCell className="table-row" />
    </TableRow>
  );
}
