export const ACTIONS = {
  SET: Symbol('SET_USERS'),
  REMOVE: Symbol('REMOVE_USERS'),
};

export default function databaseReducer(state = [], {
  type, users, id
}) {
  switch (type) {
    case ACTIONS.SET:
      return [...users];
    case ACTIONS.REMOVE:
      return state.filter(user => user.id !== id);
    default:
      return state;
  }
}
