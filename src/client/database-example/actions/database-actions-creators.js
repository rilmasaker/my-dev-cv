import { ACTIONS } from '../reducers/database-reducer';

export function setUsers(users) {
  return { type: ACTIONS.SET, users };
}

export function removeUser(id) {
  return { type: ACTIONS.REMOVE, id };
}
