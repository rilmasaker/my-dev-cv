import axios from 'axios';
import { removeUser } from './database-actions-creators';

axios.create({
  baseURL: 'http://localhost:8080/'
});

export default function destroyUser(id) {
  return dispatch => axios.delete('/api/user/delete', { data: { id } }).then(() => {
    dispatch(removeUser(id));
  });
}
