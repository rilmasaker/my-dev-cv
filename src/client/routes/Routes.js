import {
  Route, Switch, Redirect
} from 'react-router-dom';
import React from 'react';

import AboutMe from '../about-me/AboutMe';
import AboutMe2 from '../about-me/AboutMe2';
import Form from '../form/Form';
import Todo from '../rest-api-todo/ToDo';
import DatabaseTable from '../database-example/DatabaseTable';
import Login from '../login/firebase/Login';

export default function Routes() {
  return (
    <div>
      <Switch>
        <Route
          path="/about-me"
          exact
          component={AboutMe2}
        />
        <Route
          path="/form"
          exact
          component={() => <Form />}
        />
        <Route
          path="/rest-api"
          exact
          component={Todo}
        />
        <Route
          path="/database"
          exact
          component={DatabaseTable}
        />
        <Route
          path="/auth"
          exact
          component={Login}
        />
        <Redirect
          from="/"
          exact
          to="/about-me"
        />
      </Switch>
    </div>
  );
}
