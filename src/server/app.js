import express from 'express';
import os from 'os';
import bodyParser from 'body-parser';
import path from 'path';

import { createUser, findUsers, destroyUser } from '../../database/controller/controller';

const app = express();

export function routes() {
  app.get('/api/getUsername', (req, res) => res.send({ username: os.userInfo().username }));

  app.post('/api/user/create', (req, res) => {
    createUser(req).then((myUser) => {
      res.send(myUser);
    }).catch((err) => {
      res.status(500).send(`Error -> ${err}`);
    });
  });

  app.delete('/api/user/delete', (req, res) => {
    destroyUser(req).then((status) => {
      res.send(status.toString());
    }).catch((err) => {
      res.status(500).send(`Error -> ${err}`);
    });
  });

  app.get('/api/getDatabase', (req, res) => {
    findUsers().then((users) => {
      res.send(users);
    }).catch((err) => {
      res.status(500).send(`Error -> ${err}`);
    });
  });

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../dist/index.html'));
  });
}

export function useMiddleware() {
  app.use(express.static('dist'));
  app.use('/static', express.static('static'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
}

export default app;
