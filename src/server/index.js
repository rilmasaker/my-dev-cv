import models from '../../database/models';
import app, { useMiddleware, routes } from './app';


models.sequelize.sync({ force: false });
useMiddleware();
routes();
app.listen(process.env.PORT || 8080, () => console.log(`Listening on port ${process.env.PORT || 8080}!`));
