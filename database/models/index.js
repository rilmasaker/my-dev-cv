import Sequelize from 'sequelize';
import config from '../config/config.json';
import user from './user';

const db = {};

const sequelize = new Sequelize(config.database, config.username, config.password, {
  host: 'localhost',
  storage: 'db.sqlite',
  dialect: 'sqlite',
  logging: false
});

const modelModules = [user];

modelModules.forEach((modelModule) => {
  const model = modelModule(sequelize, Sequelize, config);
  db[model.name] = model;
});

Object.keys(db).forEach((modelName) => {
  if (db[modelName].asspciate) {
    db[modelName].associate(db);
  }
});

db.Sequelize = Sequelize;
db.sequelize = sequelize;

export default db;
