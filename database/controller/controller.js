import models from '../models';

export function createUser(req) {
  return models.User.create({
    name: req.body.name,
    surname: req.body.surname,
    email: req.body.email
  });
}

export function findUsers() {
 return models.User.findAll({ raw: true });
}

export function destroyUser(req) {
  return models.User.destroy({ where: { id: req.body.id } })
 }
